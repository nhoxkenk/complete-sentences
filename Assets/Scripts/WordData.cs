using System;
using UnityEngine;

[Serializable]
public class WordData
{
    public string text;
    public AudioClip clip;
}
