using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public GameState gameState;
    public static event Action<GameState> GameStateChanged;
    public MissingSentenceWords sentenceWords;
    private SetUpMissingSentences setUpMissingSentences;

    private void Start()
    {
        UpdateGameState(GameState.Choose);
        setUpMissingSentences = GetComponent<SetUpMissingSentences>();
    }

    public void UpdateGameState(GameState state)
    {
        gameState = state;
        switch (gameState)
        {
            case GameState.Choose:
                HandleChooseState();
                break;
            case GameState.Decide:
                HandleDecideState();
                break;
            case GameState.True:
                HandleChooseCorrectAnswer();
                break;
            case GameState.False:
                HandleChooseFalsetAnswer();
                break;
        }
        GameStateChanged?.Invoke(state);
    }

    private void HandleChooseState()
    {

    }

    private async void HandleDecideState()
    {
        await Task.Delay(2000);
        if(sentenceWords.isTheAnswer)
        {
            UpdateGameState(GameState.True);
        }
        else if (!sentenceWords.isTheAnswer)
        {
            UpdateGameState(GameState.False);
        }

    }

    private async void HandleChooseCorrectAnswer()
    {
        //Update next sentences
        //Reset the position of the text have choose
        if (!setUpMissingSentences.HasSentencesleft())
        {
            Debug.Log("End");
            return;
        }
        await Task.Delay(500);
        sentenceWords.ResetWordState();
        sentenceWords = null;
        setUpMissingSentences.HandleInitializing();
        UpdateGameState(GameState.Choose);
    }

    private async void HandleChooseFalsetAnswer()
    {
        await Task.Delay(500);
        sentenceWords.isClicked = false;
        await Task.Delay(2000);
        sentenceWords = null;
        UpdateGameState(GameState.Choose);
        //Wait for something here ?
    }
}
