using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : Singleton<AudioController>
{
    public AudioSource audioSource;
    public AudioClip correctAudioClip;
    public AudioClip falseAudioClip;

    public override void Awake()
    {
        base.Awake();
        audioSource = GetComponent<AudioSource>();
    }
}
