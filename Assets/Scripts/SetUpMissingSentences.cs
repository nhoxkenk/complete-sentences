
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SetUpMissingSentences : MonoBehaviour
{
    [Header("Sentence Datas")]
    [SerializeField] private List<MissingSentenceData> missingSentenceDatas;
    [SerializeField] private MissingSentenceData currentSentenceData;

    [Header("UI Components")]
    [SerializeField] private Text missingSentenceText;
    [SerializeField] private MissingSentenceWords[] sentencesMissingWords;

    [SerializeField] private int correctWordChoice;

    private void Awake()
    {
        LoadAddMissingSentenceDatas();
    }

    private void Start()
    {
        HandleInitializing();
    }

    private void LoadAddMissingSentenceDatas()
    {
        missingSentenceDatas = Resources.LoadAll<MissingSentenceData>("Sentence Datas/").ToList();
    }

    public bool HasSentencesleft()
    {
        return missingSentenceDatas.Count > 0 ? true : false;
    }

    public void HandleInitializing()
    {
        SelectNewSentence();
        SetSentenceValue();
        SetAnswersValue();
    }

    private void SelectNewSentence()
    {
        int randomIndex = Random.Range(0, missingSentenceDatas.Count);
        currentSentenceData = missingSentenceDatas[randomIndex];
        AudioController.Instance.audioSource.PlayOneShot(currentSentenceData.sentenceAudio);
        missingSentenceDatas.RemoveAt(randomIndex);
    }

    private void SetSentenceValue()
    {
        missingSentenceText.text = currentSentenceData.missingSentence;
    }

    private void SetAnswersValue()
    {
        List<WordData> words = RandomizeWords(currentSentenceData.sentenceMissingWords.ToList());

        for (int i = 0; i < sentencesMissingWords.Length; i++)
        {
            if(i == words.Count)
            {
                break;
            }

            bool isAnswer = false;
            if (i == correctWordChoice)
            {
                isAnswer = true;
            }
            sentencesMissingWords[i].SetIsAnswer(isAnswer);
            sentencesMissingWords[i].SetTextWordAnswer(words[i].text);
            sentencesMissingWords[i].SetWordAudio(words[i].clip);
        }
    }

    private List<WordData> RandomizeWords(List<WordData> list)
    {
        bool answerChoosen = false;
        List<WordData> result = new List<WordData>();

        for(int i = 0; i < sentencesMissingWords.Length;i++)
        {
            int index = Random.Range(0, list.Count);

            if(index == 0 && !answerChoosen)
            {
                answerChoosen = true;
                correctWordChoice = i;
            }

            result.Add(list[index]);
            list.RemoveAt(index);
            if(list.Count == 0)
            {
                break;
            }
        }
        return result;
    }
}
