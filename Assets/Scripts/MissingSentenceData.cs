using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Object/Missing Sentences")]
public class MissingSentenceData : ScriptableObject
{
    public string missingSentence;
    [Tooltip("Correct word should be listed first")]
    public WordData[] sentenceMissingWords;
    public AudioClip sentenceAudio;
}
