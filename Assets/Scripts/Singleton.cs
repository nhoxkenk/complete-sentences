using UnityEngine;

public class Singleton<T> : MonoBehaviour where T :Component
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if(instance == null)
            {
                instance = (T)FindObjectOfType(typeof(T));
                if (instance == null)
                    Debug.LogError("SingletoneBase<T>: Could not found GameObject of type " + typeof(T).Name);

            }
            return instance;
        }
    }

    public virtual void Awake()
    {

        if (instance == null)
        {
            instance = this as T;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
