using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissingSentenceWords : MonoBehaviour
{
    public bool isTheAnswer;
    public AudioClip wordAudio;
    [SerializeField] private Transform blackTextHolder;
    [SerializeField] private float speed = 0.5f;
    private Button wordButton;
    private Text wordText;
    public float currentTimer;
    public float targetTimer;
    public bool isClicked;

    private void Awake()
    {
        wordButton = GetComponent<Button>();
        wordText = GetComponentInChildren<Text>();
    }

    private void Start()
    {
        wordButton.onClick.AddListener(() => ClickedTheButton());
    }

    public void SetTextWordAnswer(string word)
    {
        wordText.text = word;
    }

    public void SetIsAnswer(bool isAnswer)
    {
        isTheAnswer = isAnswer;
    }

    public void SetWordAudio(AudioClip audio)
    {
        wordAudio = audio;
    }

    private void Update()
    {
        currentTimer = Mathf.MoveTowards(currentTimer, targetTimer, speed * Time.deltaTime);
        wordText.transform.position = Vector3.Lerp(transform.position, blackTextHolder.position, currentTimer);

        if (!isClicked)
        {
            targetTimer = 0;
        }
    }

    public void ClickedTheButton()
    {
        GameManager.Instance.sentenceWords = this;
        GameManager.Instance.UpdateGameState(GameState.Decide);
        AudioController.Instance.audioSource.PlayOneShot(wordAudio);
        targetTimer = targetTimer == 0 ? 1 : 0;
        isClicked = true;
    }

    public void ResetWordState()
    {
        isClicked = false;
        currentTimer = 0;
        targetTimer = 0;
        wordText.transform.position = blackTextHolder.position;
    }
}
